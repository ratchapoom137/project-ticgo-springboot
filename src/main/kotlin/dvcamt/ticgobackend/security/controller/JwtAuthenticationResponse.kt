package dvcamt.ticgobackend.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)