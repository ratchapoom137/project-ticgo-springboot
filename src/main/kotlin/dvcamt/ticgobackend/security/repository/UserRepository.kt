package dvcamt.ticgobackend.security.repository

import dvcamt.ticgobackend.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}