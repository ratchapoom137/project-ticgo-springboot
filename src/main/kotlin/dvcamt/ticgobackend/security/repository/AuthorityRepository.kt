package dvcamt.ticgobackend.security.repository

import dvcamt.ticgobackend.security.entity.Authority
import dvcamt.ticgobackend.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}