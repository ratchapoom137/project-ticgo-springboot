package dvcamt.ticgobackend.entity

enum class SeatType {
    PREMIUM,SOFA,DELUXE
}