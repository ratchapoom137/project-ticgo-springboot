package dvcamt.ticgobackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class UserData(
        override var email: String? = null,
        override var password: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null,
        var isDeleted: Boolean = false
) : User(email, password) {
    @OneToMany
    var booking = mutableListOf<Booking>()
}
