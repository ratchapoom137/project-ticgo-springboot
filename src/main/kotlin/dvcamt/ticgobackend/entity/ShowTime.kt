package dvcamt.ticgobackend.entity

import java.sql.Timestamp
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import javax.persistence.*

@Entity
data class ShowTime(var startDateTime: Long? = null,
                    var endDateTime: Long? = null,
                    var soundtrack: SubtitleAndSoundtrack = SubtitleAndSoundtrack.EN,
                    var subtitle: SubtitleAndSoundtrack = SubtitleAndSoundtrack.EN) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToOne
    lateinit var movie: Movie
    @OneToOne
    lateinit var cinema: Cinema
    @OneToMany
    var seats = mutableListOf<SelectedSeat>()
}