package dvcamt.ticgobackend.entity

import java.time.LocalDate
import javax.persistence.*

@Entity
data class Booking(var showTimeId: Long? = null,
                   var createdDateTime: Long? = null,
                   var userId: Long? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToMany
    var seats = mutableListOf<SeatInShowTime>()
}