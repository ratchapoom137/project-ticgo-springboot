package dvcamt.ticgobackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Movie(var name: String? = null,
                 var duration: Double? = null,
                 var image: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToMany
    var subtitle = mutableListOf<SubtitleMovie>()
    @ManyToMany
    var soundtrackMovie = mutableListOf<SoundtrackMovie>()
}