package dvcamt.ticgobackend.entity

import dvcamt.ticgobackend.security.entity.JwtUser
import javax.persistence.*

//interface User{
//    var email: String
//    var password: String
//}
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User(
        open var email: String? = null,
        open var password: String? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var jwtUser: JwtUser? = null
}