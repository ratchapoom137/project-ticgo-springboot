package dvcamt.ticgobackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class SubtitleMovie(var subtitle: SubtitleAndSoundtrack = SubtitleAndSoundtrack.EN) {
    @Id
    @GeneratedValue
    var id:Long? = null
}