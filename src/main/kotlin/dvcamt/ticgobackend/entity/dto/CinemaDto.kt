package dvcamt.ticgobackend.dao.dto

data class CinemaDto(
        var name: String? = null,
        var seat: List<SeatDto>?  = null
)