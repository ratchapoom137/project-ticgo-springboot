package dv.spring.kotlin.entity.dto

data class UserDto(
        var email: String? = null,
        var password: String? = null,
        var username: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var authorities: List<AuthorityDto> = mutableListOf()
)
