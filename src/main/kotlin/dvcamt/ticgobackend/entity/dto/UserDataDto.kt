package dvcamt.ticgobackend.entity.dto

data class UserDataDto(
        var email: String? = null,
        var password: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
)