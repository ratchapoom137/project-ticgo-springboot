package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.SeatType

data class SeatDto(
        var name: String? = null,
        var type: SeatType? = null,
        var price: Double? = null,
        var rows: Int? = null,
        var column: Int? = null
)