package dvcamt.ticgobackend.entity.dto

data class UserDataRegisterDto(
        var email: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var image: String? = null
)