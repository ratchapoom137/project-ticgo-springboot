package dvcamt.ticgobackend.entity.dto

data class UserInfoDto(
        var firstName:String?=null,
        var lastName:String?=null,
        var id:Long?=null
)

data class UserImageDto(
        var image:String?=null,
        var id:Long?=null
)