package dvcamt.ticgobackend.dao.dto

data class MovieDto(
        var name: String? = null,
        var duration: Double? = null,
        var image: String? = null
)