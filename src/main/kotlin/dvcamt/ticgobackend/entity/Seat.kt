package dvcamt.ticgobackend.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Seat(var name: String? = null,
                var type: SeatType? = null,
                var price: Double? = null,
                @Column(name = "Seat_rows")
                var rows: Int? = null,
                @Column(name = "Seat_columns")
                var columns: Int? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
}