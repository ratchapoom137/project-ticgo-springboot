package dvcamt.ticgobackend.controller

import dvcamt.ticgobackend.entity.UserData
import dvcamt.ticgobackend.entity.dto.UserDataDto
import dvcamt.ticgobackend.security.entity.AuthorityName
import dvcamt.ticgobackend.security.entity.JwtUser
import dvcamt.ticgobackend.security.repository.AuthorityRepository
import dvcamt.ticgobackend.security.repository.UserRepository
import dvcamt.ticgobackend.service.AmazonClient
import dvcamt.ticgobackend.service.UserService
import dvcamt.ticgobackend.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class UserController{
    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @GetMapping("/user")
    fun getAllUser(): ResponseEntity<Any> {
        val users = userService.getAllUser()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDataDto(users))
    }

    @Autowired
    lateinit var amazonClient: AmazonClient

    @PostMapping("/user/image/{userId}")
    fun uploadFile(@RequestPart(value = "file") file: MultipartFile,
                   @PathVariable("userId")id:Long ): ResponseEntity<*> {
        var imageUrl = this.amazonClient.uploadFile(file)
        var userId = userService.saveImage(id,imageUrl)
        val output= MapperUtil.INSTANCE.mapUserImageDto(userId)
        return ResponseEntity.ok(output)
    }

    @PostMapping("/user/register")
    fun register(@RequestBody userDataDto: UserDataDto):ResponseEntity<Any>{
        val output = userService.save(MapperUtil.INSTANCE.mapUserDataDto(userDataDto))
        val outputDto = MapperUtil.INSTANCE.mapUserDataRegisterDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PutMapping("/user/edit")
    fun editUser(@RequestBody user: UserData):ResponseEntity<Any>{
        val output= userService.editUser(user)
        val outputDto = MapperUtil.INSTANCE.mapUserDataDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }

    @GetMapping("/user/history/booking/{userId}")
    fun getBookingByUserId(@PathVariable("userId")id:Long) : ResponseEntity<Any> {
        var output = userService.getBookingByUserId(id)
//        var output = MapperUtil.INSTANCE.mapBooking(userService.getBookingByUserId(id))
        return ResponseEntity.ok(output)
    }

    @DeleteMapping("/user/delete/{userId}")
    fun deletedUserById(@PathVariable("userId")id:Long): ResponseEntity<Any> {
        var output = userService.deletedUserById(id)
        var outputDto = MapperUtil.INSTANCE.mapUserDataDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This user is not found!")
    }
}