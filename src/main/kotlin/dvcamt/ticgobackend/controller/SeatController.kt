package dvcamt.ticgobackend.controller

import dvcamt.ticgobackend.service.SeatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SeatController{
    @Autowired
    lateinit var seatService: SeatService

    @GetMapping("/seat")
    fun getAllSeats(): ResponseEntity<Any> {
        return ResponseEntity.ok(seatService.getSeats())
    }
}