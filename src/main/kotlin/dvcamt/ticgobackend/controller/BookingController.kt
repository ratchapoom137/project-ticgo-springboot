package dvcamt.ticgobackend.controller

import dvcamt.ticgobackend.entity.Booking
import dvcamt.ticgobackend.service.BookingService
import dvcamt.ticgobackend.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class BookingController{
    @Autowired
    lateinit var bookingService: BookingService
    @Autowired
    lateinit var userService: UserService

    @PostMapping("/booking")
    fun addBooking(@RequestBody booking: Booking): ResponseEntity<Any> {
        val bookings = bookingService.save(booking)
//        userService.saveBooking(booking)
        return ResponseEntity.ok(bookings)
    }
}