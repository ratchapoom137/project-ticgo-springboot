package dvcamt.ticgobackend.controller

import dvcamt.ticgobackend.service.AmazonClient
import dvcamt.ticgobackend.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController
class BucketController {
    @Autowired
    lateinit var amazonClient: AmazonClient
    @Autowired
    lateinit var userDataService: UserService

    @PostMapping("/uploadFile")
    fun uploadFile(@RequestPart(value = "file") file: MultipartFile): ResponseEntity<*> {
        return ResponseEntity.ok(this.amazonClient.uploadFile(file))
    }

    @DeleteMapping("/deleteFile")
    fun deleteFile(@RequestPart(value = "url") fileUrl: String): ResponseEntity<*> {
        return ResponseEntity.ok(this.amazonClient.deleteFileFromS3Bucket(fileUrl))
    }
}
