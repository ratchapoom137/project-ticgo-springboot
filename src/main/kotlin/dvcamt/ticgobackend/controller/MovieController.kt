package dvcamt.ticgobackend.controller

import dvcamt.ticgobackend.service.MovieService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MovieController{
    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/movie")
    fun getAllMovie(): ResponseEntity<Any> {
        val movies = movieService.getAllMovie()
        return ResponseEntity.ok(movies)
    }
}