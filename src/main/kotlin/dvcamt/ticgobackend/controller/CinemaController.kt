package dvcamt.ticgobackend.controller

import dvcamt.ticgobackend.service.CinemaService
import dvcamt.ticgobackend.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class CinemaController{
    @Autowired
    lateinit var cinemaService: CinemaService

    @GetMapping("/cinema")
    fun getAllCinema(): ResponseEntity<Any> {
        val cinemas = cinemaService.getAllCinema()
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCinemaDto(cinemas))
        return  ResponseEntity.ok(cinemas)
    }
}