package dvcamt.ticgobackend.controller

import dvcamt.ticgobackend.entity.ShowTime
import dvcamt.ticgobackend.service.ShowTimeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShowTimeController{
    @Autowired
    lateinit var showTimeService: ShowTimeService

    @GetMapping("/showTime")
    fun getAllShowTime(): ResponseEntity<Any> {
        val showTimes = showTimeService.getAllShowTime()
        return ResponseEntity.ok(showTimes)
    }

    @PostMapping("/showTime/add")
    fun addShowTime(@RequestBody showtime: ShowTime): ResponseEntity<Any> {
        val showTime = showTimeService.saveShowTime(showtime)
        return ResponseEntity.ok(showTime)
    }

    @GetMapping("/showTime/{id}")
    fun getOneShowTime(@PathVariable("id")id: Long): ResponseEntity<Any> {
        var output = showTimeService.getOneShowTime(id)
        return ResponseEntity.ok(output)
    }

    @GetMapping("/showTime/movie/{id}")
    fun getShowTimeByMovieId(@PathVariable("id")id: Long): ResponseEntity<Any> {
        var output = showTimeService.getShowTimeByMovieId(id)
        return ResponseEntity.ok(output)
    }
}