package dvcamt.ticgobackend.config

import dvcamt.ticgobackend.entity.*
import dvcamt.ticgobackend.repository.*
import dvcamt.ticgobackend.security.entity.Authority
import dvcamt.ticgobackend.security.entity.AuthorityName
import dvcamt.ticgobackend.security.entity.JwtUser
import dvcamt.ticgobackend.security.repository.AuthorityRepository
import dvcamt.ticgobackend.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.sql.Timestamp
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var bookingRepository: BookingRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var seatInShowTimeRepository: SeatInShowTimeRepository
    @Autowired
    lateinit var showTimeRepository: ShowTimeRepository
    @Autowired
    lateinit var soundtrackMovieRepository: SoundtrackMovieRepository
    @Autowired
    lateinit var subtitleRepository: SubtitleMovieRepository
    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository
    @Autowired
    lateinit var userDataRepository: UserDataRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val custl = UserData(email = "testpoom@gmail.com", password = "password", firstName = "testpoooom", lastName = "eie")
        val custJwt = JwtUser(
                username = custl.email,
                password = encoder.encode(custl.password),
                email = custl.email,
                enabled = true,
                firstname = custl.firstName,
                lastname = custl.lastName
        )
        userDataRepository.save(custl)
        userRepository.save(custJwt)
        custl.jwtUser = custJwt
        custJwt.user = custl
        custJwt.authorities.add(auth1)
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)
    }

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var movie1 = movieRepository.save(Movie("อลิตา แบทเทิล แองเจิ้ล", 125.00
                , "https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024"))
        var movie2 = movieRepository.save(Movie("อภินิหารไวกิ้งพิชิตมังกร 3", 105.00
                , "https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024"))
        var movie3 = movieRepository.save(Movie("Captain Marvel", 130.00
                , "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024"))
        var movie4 = movieRepository.save(Movie("เฟรนด์โซน ระวัง..สิ้นสุดทางเพื่อน", 120.00
                , "https://lh3.googleusercontent.com/hGX4-13iqHM1Kx_nyj67AtCsFXAjJyS0wxoEcJynOs99BUJXQWLIUQFcsje3jdMW75wtrJU2ktywEF3gxoKe=w1024"))
        var movie5 = movieRepository.save(Movie("สุขสันต์วันตาย 2U", 100.00
                , "https://lh3.googleusercontent.com/8hKivfDWOC2G8rQolX9850yiPWs5dJzSgyGirJFjU66jeUPV9-5gkm1soLxlHubI5_V_lK1T0vdfinsUlVZtOA=w1024"))
        var movie6 = movieRepository.save(Movie("คุซามะ อินฟินิตี้", 80.00
                , "https://lh3.googleusercontent.com/Fs3-9D0HjOaP-BWhOhVp-gZAqN3aRyyB5Z7W59f67v_frABpaljFUhFR9Pjs-fTN37jz1mqml9LRUK1tVjjFBg=w1024"))
        var movie7 = movieRepository.save(Movie("คนเหนือมนุษย์", 130.00
                , "https://lh3.googleusercontent.com/rVCTrkvoC4Iwpv4sPY8gSGYom0rKm28exIaVBD2sGYUip0SMuKqfDxyAVrd_Ps_H39Ss-NGT1nzytQPuBunr=w1024"))

        var soundtrackEN = soundtrackMovieRepository.save(SoundtrackMovie(SubtitleAndSoundtrack.EN))
        var soundtrackTH = soundtrackMovieRepository.save(SoundtrackMovie(SubtitleAndSoundtrack.TH))

        var subtitleEN = subtitleRepository.save(SubtitleMovie(SubtitleAndSoundtrack.EN))
        var subtitleTH = subtitleRepository.save(SubtitleMovie(SubtitleAndSoundtrack.TH))

        movie1.soundtrackMovie.add(soundtrackEN)
        movie1.soundtrackMovie.add(soundtrackTH)
        movie1.subtitle.add(subtitleTH)
        movie2.soundtrackMovie.add(soundtrackEN)
        movie2.soundtrackMovie.add(soundtrackTH)
        movie2.subtitle.add(subtitleTH)
        movie3.soundtrackMovie.add(soundtrackEN)
        movie3.soundtrackMovie.add(soundtrackTH)
        movie3.subtitle.add(subtitleTH)
        movie4.soundtrackMovie.add(soundtrackTH)
        movie4.subtitle.add(subtitleEN)
        movie5.soundtrackMovie.add(soundtrackEN)
        movie5.soundtrackMovie.add(soundtrackTH)
        movie5.subtitle.add(subtitleTH)
        movie6.soundtrackMovie.add(soundtrackEN)
        movie6.subtitle.add(subtitleTH)
        movie7.soundtrackMovie.add(soundtrackEN)
        movie7.soundtrackMovie.add(soundtrackTH)
        movie7.subtitle.add(subtitleTH)

        var premium = seatRepository.save(Seat("Premium", SeatType.PREMIUM, 190.00, 4, 20))
        var deluxe = seatRepository.save(Seat("Deluxe", SeatType.DELUXE, 150.00, 10, 20))
        var sofa = seatRepository.save(Seat("Sofa Sweet (Pair)", SeatType.SOFA, 500.00, 1, 6))

        var cinema1 = cinemaRepository.save(Cinema("Cinema 1"))
        var cinema2 = cinemaRepository.save(Cinema("Cinema 2"))
        var cinema3 = cinemaRepository.save(Cinema("Cinema 3"))

        cinema1.seat = mutableListOf(sofa, premium, deluxe)
        cinema2.seat = mutableListOf(premium, deluxe)
        cinema3.seat = mutableListOf(sofa, premium, deluxe)

        var showTime1 = showTimeRepository.save(ShowTime(Timestamp(1553319600000).time, Timestamp(1553328900000).time, SubtitleAndSoundtrack.EN, SubtitleAndSoundtrack.TH))
        showTime1.movie = movie1
        showTime1.cinema = cinema3
        var row: Char = 'A'
        for ((index, item) in showTime1.cinema.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatInShowTime
                    if( item.type === SeatType.SOFA ){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showTime1.seats.add(selectedSeat)
            if( item.type === SeatType.SOFA ){
                row = 'A'
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var showTime2 = showTimeRepository.save(ShowTime(Timestamp(1553328900000).time, Timestamp(1553335500000).time, SubtitleAndSoundtrack.EN, SubtitleAndSoundtrack.TH))
        showTime2.movie = movie6
        showTime2.cinema = cinema2
        row = 'A'
        for ((index, item) in showTime2.cinema.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatInShowTime
                    if( item.type === SeatType.SOFA ){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showTime2.seats.add(selectedSeat)
            if( item.type === SeatType.SOFA ){
                row = 'A'
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var showTime3 = showTimeRepository.save(ShowTime(Timestamp(1553342100000).time, Timestamp(1553351700000).time, SubtitleAndSoundtrack.EN, SubtitleAndSoundtrack.TH))
        showTime3.movie = movie3
        showTime3.cinema = cinema3
        row = 'A'
        for ((index, item) in showTime3.cinema.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatInShowTime
                    if( item.type === SeatType.SOFA ){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showTime3.seats.add(selectedSeat)
            if( item.type === SeatType.SOFA ){
                row = 'A'
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var showTime4 = showTimeRepository.save(ShowTime(Timestamp(1553351700000).time, Timestamp(1553361300000).time, SubtitleAndSoundtrack.EN, SubtitleAndSoundtrack.TH))
        showTime4.movie = movie2
        showTime4.cinema = cinema1
        row = 'A'
        for ((index, item) in showTime4.cinema.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatInShowTime
                    if( item.type === SeatType.SOFA ){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showTime4.seats.add(selectedSeat)
            if( item.type === SeatType.SOFA ){
                row = 'A'
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var user1 = userDataRepository.save(UserData("poom@gmail.com", "1234", "Ratchapoom", "Rattanakit", "https://www.img.in.th/images/4dc3d85d6b0bfce7fa024305d7fff670.md.jpg"))
        loadUsernameAndPassword()
    }
}
