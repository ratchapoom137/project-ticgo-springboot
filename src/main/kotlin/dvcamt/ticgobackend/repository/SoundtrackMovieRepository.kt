package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.SoundtrackMovie
import org.springframework.data.repository.CrudRepository

interface SoundtrackMovieRepository : CrudRepository<SoundtrackMovie, Long> {

}