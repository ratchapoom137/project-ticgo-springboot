package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.UserData
import org.springframework.data.repository.CrudRepository

interface UserDataRepository: CrudRepository<UserData, Long> {
    fun findByIsDeletedIsFalse(): List<UserData>
}