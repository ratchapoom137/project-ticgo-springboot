package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.Booking
import org.springframework.data.repository.CrudRepository

interface BookingRepository : CrudRepository<Booking, Long> {
    fun findByUserId(id: Long): List<Booking>
}