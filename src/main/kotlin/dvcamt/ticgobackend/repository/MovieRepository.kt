package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.Movie
import org.springframework.data.repository.CrudRepository

interface MovieRepository : CrudRepository<Movie, Long> {

}