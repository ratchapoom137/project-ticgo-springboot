package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.SeatInShowTime
import org.springframework.data.repository.CrudRepository

interface SeatInShowTimeRepository : CrudRepository<SeatInShowTime, Long> {

}