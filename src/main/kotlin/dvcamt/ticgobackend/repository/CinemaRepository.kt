package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.Cinema
import org.springframework.data.repository.CrudRepository

interface CinemaRepository : CrudRepository<Cinema, Long> {

}