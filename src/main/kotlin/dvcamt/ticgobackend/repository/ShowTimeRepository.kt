package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.ShowTime
import org.springframework.data.repository.CrudRepository

interface ShowTimeRepository : CrudRepository<ShowTime, Long> {
    fun findByMovie_id(id: Long): List<ShowTime>

}