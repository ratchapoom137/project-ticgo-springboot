package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.Seat
import dvcamt.ticgobackend.entity.SeatInShowTime
import org.springframework.data.repository.CrudRepository

interface SeatRepository : CrudRepository<Seat, Long> {
//    fun findByRowContainingIgnoreCaseAndColumnContainingIgnoreCase(): SeatInShowTime
}