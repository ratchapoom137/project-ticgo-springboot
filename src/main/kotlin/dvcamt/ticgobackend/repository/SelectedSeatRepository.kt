package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.SelectedSeat
import org.springframework.data.repository.CrudRepository

interface SelectedSeatRepository : CrudRepository<SelectedSeat, Long> {

}