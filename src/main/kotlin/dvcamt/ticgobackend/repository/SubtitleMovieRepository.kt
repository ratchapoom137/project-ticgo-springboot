package dvcamt.ticgobackend.repository

import dvcamt.ticgobackend.entity.SubtitleMovie
import org.springframework.data.repository.CrudRepository

interface SubtitleMovieRepository : CrudRepository<SubtitleMovie, Long> {

}