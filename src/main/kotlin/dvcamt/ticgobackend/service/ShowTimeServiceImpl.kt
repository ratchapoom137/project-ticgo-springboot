package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.dao.dto.CinemaDao
import dvcamt.ticgobackend.dao.dto.MovieDao
import dvcamt.ticgobackend.dao.dto.ShowTimeDao
import dvcamt.ticgobackend.entity.SeatInShowTime
import dvcamt.ticgobackend.entity.SeatType
import dvcamt.ticgobackend.entity.SelectedSeat
import dvcamt.ticgobackend.entity.ShowTime
import dvcamt.ticgobackend.repository.SeatInShowTimeRepository
import dvcamt.ticgobackend.repository.SelectedSeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShowTimeServiceImpl: ShowTimeService{
    override fun getShowTimeByMovieId(id: Long): List<ShowTime> {
        return showTimeDao.findByMovieId(id)
    }

    override fun getOneShowTime(id: Long): ShowTime {
        return showTimeDao.findById(id)
    }

    override fun saveShowTime(showtime: ShowTime): ShowTime {
        var showTimeNew = ShowTime()
        var movie = movieDao.findById(showtime.movie.id)
        var cinema = cinemaDao.findById(showtime.cinema.id)
        showTimeNew.cinema = cinema
        showTimeNew.movie = movie
        showTimeNew.startDateTime = showtime.startDateTime
        showTimeNew.endDateTime = showtime.endDateTime
        showTimeNew.soundtrack = showtime.soundtrack
        showTimeNew.subtitle = showtime.subtitle
        var showTimeSaved = showTimeDao.save(showTimeNew)
        var row: Char = 'A'
        for((index, item) in showTimeSaved.cinema.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatInShowTime
                    if( item.type === SeatType.SOFA ){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showTimeSaved.seats.add(selectedSeat)
            if( item.type === SeatType.SOFA ){
                row = 'A'
            }
        }
        return showTimeDao.save(showTimeSaved)
    }

    override fun getAllShowTime(): List<ShowTime> {
        return showTimeDao.getAllShowTime()
    }
    @Autowired
    lateinit var seatInShowTimeRepository: SeatInShowTimeRepository
    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository

    @Autowired
    lateinit var cinemaDao: CinemaDao

    @Autowired
    lateinit var movieDao: MovieDao

    @Autowired
    lateinit var showTimeDao: ShowTimeDao
}