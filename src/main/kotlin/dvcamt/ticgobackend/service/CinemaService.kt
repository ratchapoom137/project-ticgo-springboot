package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.entity.Cinema

interface CinemaService{
    fun getAllCinema(): List<Cinema>
}