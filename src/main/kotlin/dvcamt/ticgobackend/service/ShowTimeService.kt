package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.entity.ShowTime

interface ShowTimeService{
    fun getAllShowTime(): List<ShowTime>
    fun saveShowTime(showtime: ShowTime): ShowTime
    fun getOneShowTime(id: Long): ShowTime
    fun getShowTimeByMovieId(id: Long): List<ShowTime>
}