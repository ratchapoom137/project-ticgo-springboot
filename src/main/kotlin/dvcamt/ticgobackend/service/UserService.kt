package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.entity.Booking
import dvcamt.ticgobackend.entity.UserData

interface UserService{
    fun getAllUser(): List<UserData>
    fun saveImage(id: Long, imageUrl: String): UserData
    fun save(user: UserData): UserData
    fun editUser(user: UserData): UserData
    fun saveBooking(booking: Booking): UserData
    fun getBookingByUserId(id: Long): List<Booking>
    fun deletedUserById(id: Long): UserData
}