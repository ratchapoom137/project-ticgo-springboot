package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.entity.Seat

interface SeatService{
    fun getSeats(): List<Seat>
}