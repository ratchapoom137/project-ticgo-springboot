package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.entity.Booking

interface BookingService{
    fun save(booking: Booking): Booking
}