package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.entity.Movie

interface MovieService{
    fun getAllMovie(): List<Movie>
}