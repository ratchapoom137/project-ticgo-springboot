package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.dao.dto.SeatDao
import dvcamt.ticgobackend.entity.Seat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SeatServiceImpl: SeatService{
    override fun getSeats(): List<Seat> {
        return seatDao.getAllSeats()
    }

    @Autowired
    lateinit var seatDao: SeatDao
}