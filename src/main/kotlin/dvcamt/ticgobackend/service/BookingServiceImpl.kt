package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.dao.dto.BookingDao
import dvcamt.ticgobackend.dao.dto.SeatDao
import dvcamt.ticgobackend.dao.dto.SeatInShowTimeDao
import dvcamt.ticgobackend.dao.dto.ShowTimeDao
import dvcamt.ticgobackend.entity.Booking
import dvcamt.ticgobackend.entity.SeatInShowTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.awt.print.Book

@Service
class BookingServiceImpl: BookingService {
    override fun save(booking: Booking): Booking {
        var showTimeId = showTimeDao.findById(booking.showTimeId)
        var seatMap = mutableListOf<SeatInShowTime>()
        for( item in booking.seats) {
            var seat = seatInShowTimeDao.findById(item.id)
            seat.status = true
            seatMap.add(seatInShowTimeDao.save(seat))
        }
        var book = Booking()
        book.showTimeId = showTimeId.id
        book.seats = seatMap
        book.createdDateTime = System.currentTimeMillis()
        book.userId = booking.userId
        return bookingDao.save(book)
    }

    @Autowired
    lateinit var seatInShowTimeDao: SeatInShowTimeDao

    @Autowired
    lateinit var showTimeDao: ShowTimeDao

    @Autowired
    lateinit var bookingDao: BookingDao
}