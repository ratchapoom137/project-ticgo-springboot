package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.dao.dto.BookingDao
import dvcamt.ticgobackend.dao.dto.UserDao
import dvcamt.ticgobackend.entity.Booking
import dvcamt.ticgobackend.entity.User
import dvcamt.ticgobackend.entity.UserData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl: UserService{
    override fun deletedUserById(id: Long): UserData {
        var user = userDao.findById(id)
        user?.isDeleted = true
        return userDao.save(user)
    }

    override fun getBookingByUserId(id: Long): List<Booking> {
        return bookingDao.findByUserId(id)
    }

    override fun saveBooking(booking: Booking): UserData {
        val userMap = userDao.findById(booking.userId!!)
        userMap.booking.add(booking)
        return userDao.saveBooking(userMap)
    }

    override fun editUser(user: UserData): UserData {
        val userMap = userDao.findById(user.id!!)
        userMap.firstName = user.firstName
        userMap.lastName = user.lastName
        return userDao.save(userMap)
    }

    override fun save(user: UserData): UserData {
        return userDao.save(user)
    }

    override fun saveImage(id: Long, imageUrl: String): UserData {
        val user = userDao.findById(id)
        user.image = imageUrl
        return userDao.save(user)
    }

    override fun getAllUser(): List<UserData> {
        return userDao.getAllUser()
    }

    @Autowired
    lateinit var bookingDao: BookingDao

    @Autowired
    lateinit var userDao: UserDao
}