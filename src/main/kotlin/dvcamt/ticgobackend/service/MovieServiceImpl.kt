package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.dao.dto.MovieDao
import dvcamt.ticgobackend.entity.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MovieServiceImpl: MovieService{
    override fun getAllMovie(): List<Movie> {
        return movieDao.getALlMovie()
    }

    @Autowired
    lateinit var movieDao: MovieDao
}