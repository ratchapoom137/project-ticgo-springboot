package dvcamt.ticgobackend.service

import dvcamt.ticgobackend.dao.dto.CinemaDao
import dvcamt.ticgobackend.entity.Cinema
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CinemaServiceImpl: CinemaService {
    override fun getAllCinema(): List<Cinema> {
        return cinemaDao.getAllCinema()
    }

    @Autowired
    lateinit var cinemaDao: CinemaDao
}