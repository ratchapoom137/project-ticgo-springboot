package dvcamt.ticgobackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TicgoBackendApplication

fun main(args: Array<String>) {
	runApplication<TicgoBackendApplication>(*args)
}
