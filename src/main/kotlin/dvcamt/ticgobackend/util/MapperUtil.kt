package dvcamt.ticgobackend.util

import dv.spring.kotlin.entity.dto.AuthorityDto
import dv.spring.kotlin.entity.dto.UserDto
//import dv.spring.kotlin.entity.dto.UserImageDto
import dvcamt.ticgobackend.dao.dto.CinemaDto
import dvcamt.ticgobackend.dao.dto.SeatDto
import dvcamt.ticgobackend.entity.Booking
import dvcamt.ticgobackend.entity.Cinema
import dvcamt.ticgobackend.entity.Seat
import dvcamt.ticgobackend.entity.UserData
import dvcamt.ticgobackend.entity.dto.UserDataDto
import dvcamt.ticgobackend.entity.dto.UserDataRegisterDto
import dvcamt.ticgobackend.entity.dto.UserImageDto
import dvcamt.ticgobackend.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapCinemaDto(cinema: Cinema): CinemaDto
    fun mapCinemaDto(cinema: List<Cinema>): List<CinemaDto>

    fun mapUserDataDto(userData: UserData): UserDataDto
    fun mapUserDataDto(userData: List<UserData>): List<UserDataDto>

    fun mapUserDataRegisterDto(userData: UserData): UserDataRegisterDto

    @InheritInverseConfiguration
    fun mapUserDataDto(userData: UserDataDto): UserData

    fun mapSeatDto(seat: Seat): SeatDto
    fun mapSeatDto(seat: List<Seat>): List<SeatDto>
    fun mapUserImageDto(userData: UserData): UserImageDto
    @Mappings(
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
    )
    fun mapUser(customer: UserData): UserDto

    fun mapAuthority(authority: AuthorityDto): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>

    fun mapBooking(bookingByUserId: Booking): Booking
    fun mapBooking(bookingByUserId: List<Booking>): List<Booking>

}