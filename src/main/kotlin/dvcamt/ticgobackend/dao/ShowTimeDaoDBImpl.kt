package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.ShowTime
import dvcamt.ticgobackend.repository.ShowTimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShowTimeDaoDBImpl: ShowTimeDao {
    override fun findByMovieId(id: Long): List<ShowTime> {
        return showTimeRepository.findByMovie_id(id)
    }

    override fun save(showTimeNew: ShowTime): ShowTime {
        return showTimeRepository.save(showTimeNew)
    }

    override fun findById(showTimeId: Long?): ShowTime {
        return showTimeRepository.findById(showTimeId!!).orElse(null)
    }

    override fun getAllShowTime(): List<ShowTime> {
        return showTimeRepository.findAll().filterIsInstance(ShowTime::class.java)
    }

    @Autowired
    lateinit var showTimeRepository: ShowTimeRepository
}