package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Booking
import dvcamt.ticgobackend.repository.BookingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class BookingDaoDBImpl:BookingDao {
    override fun findByUserId(id: Long): List<Booking> {
        return bookingRepository.findByUserId(id)
    }

    override fun save(book: Booking): Booking {
        return bookingRepository.save(book)
    }

    @Autowired
    lateinit var bookingRepository: BookingRepository
}