package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.repository.SoundtrackMovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SoundtrackMovieDaoDBImpl: SoundtrackMovieDao {
    @Autowired
    lateinit var soundtrackMovieRepository: SoundtrackMovieRepository
}