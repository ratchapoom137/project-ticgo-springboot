package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Movie

interface MovieDao {
    fun getALlMovie(): List<Movie>
    fun findById(id: Long?): Movie

}