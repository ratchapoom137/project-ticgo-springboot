package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.SeatInShowTime

interface SeatInShowTimeDao {
    fun findById(id: Long?): SeatInShowTime
    fun save(seat: SeatInShowTime): SeatInShowTime

}