package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.User
import dvcamt.ticgobackend.entity.UserData

interface UserDao {
    fun getAllUser(): List<UserData>
    fun findById(id: Long): UserData
    fun save(user: UserData): UserData
    fun saveBooking(userMap: UserData): UserData

}