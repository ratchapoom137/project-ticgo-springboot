package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.User
import dvcamt.ticgobackend.entity.UserData
import dvcamt.ticgobackend.repository.UserDataRepository
import dvcamt.ticgobackend.security.entity.AuthorityName
import dvcamt.ticgobackend.security.entity.JwtUser
import dvcamt.ticgobackend.security.repository.AuthorityRepository
import dvcamt.ticgobackend.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoDBImpl: UserDao {
    override fun saveBooking(userMap: UserData): UserData {
        return userDataRepository.save(userMap)
    }

    override fun save(user: UserData): UserData {
        val authority = authorityRepository.findByName(AuthorityName.ROLE_CUSTOMER)
        val encoder = BCryptPasswordEncoder()
        val jwt = userRepository.save(JwtUser(user.email, user.firstName, user.lastName, encoder.encode(user.password), user.email, true))
        val userSave = userDataRepository.save(user)
        jwt.user = userSave
        userSave.jwtUser = jwt
        jwt.authorities.add(authority)
        return userDataRepository.save(user)
    }

    override fun findById(id: Long): UserData {
        return userDataRepository.findById(id).orElse(null)
    }

    override fun getAllUser(): List<UserData> {
//        return userDataRepository.findAll().filterIsInstance(UserData::class.java)
        return userDataRepository.findByIsDeletedIsFalse()
    }
    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var userDataRepository: UserDataRepository
}