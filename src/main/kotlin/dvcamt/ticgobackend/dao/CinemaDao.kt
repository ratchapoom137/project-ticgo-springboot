package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Cinema

interface CinemaDao {
    fun getAllCinema(): List<Cinema>
    fun findById(id: Long?): Cinema
}