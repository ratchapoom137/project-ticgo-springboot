package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Movie
import dvcamt.ticgobackend.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class MovieDaoDBImpl: MovieDao {
    override fun findById(id: Long?): Movie {
        return movieRepository.findById(id!!).orElse(null)
    }

    override fun getALlMovie(): List<Movie> {
        return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }

    @Autowired
    lateinit var movieRepository: MovieRepository
}