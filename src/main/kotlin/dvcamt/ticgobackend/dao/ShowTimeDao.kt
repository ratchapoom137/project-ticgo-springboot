package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.ShowTime

interface ShowTimeDao {
    fun getAllShowTime(): List<ShowTime>
    fun findById(showTimeId: Long?): ShowTime
    fun save(showTimeNew: ShowTime): ShowTime
    fun findByMovieId(id: Long): List<ShowTime>
}