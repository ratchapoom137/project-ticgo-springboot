package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Booking

interface BookingDao {
    fun save(book: Booking): Booking
    fun findByUserId(id: Long): List<Booking>

}