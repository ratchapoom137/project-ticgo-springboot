package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Cinema
import dvcamt.ticgobackend.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CinemaDaoDBImpl: CinemaDao {
    override fun findById(id: Long?): Cinema {
        return cinemaRepository.findById(id!!).orElse(null)
    }

    override fun getAllCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }

    @Autowired
    lateinit var cinemaRepository: CinemaRepository
}