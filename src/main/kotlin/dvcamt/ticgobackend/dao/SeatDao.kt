package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Seat

interface SeatDao {
    fun getAllSeats(): List<Seat>
}