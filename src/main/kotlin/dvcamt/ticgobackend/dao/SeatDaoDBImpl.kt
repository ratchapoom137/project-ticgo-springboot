package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.Seat
import dvcamt.ticgobackend.repository.SeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SeatDaoDBImpl: SeatDao {
    override fun getAllSeats(): List<Seat> {
        return seatRepository.findAll().filterIsInstance(Seat::class.java)
    }

    @Autowired
    lateinit var seatRepository: SeatRepository
}