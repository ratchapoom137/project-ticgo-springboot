package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.repository.SelectedSeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SelectedSeatDaoDBImpl: SelectedSeatDao {
    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository
}