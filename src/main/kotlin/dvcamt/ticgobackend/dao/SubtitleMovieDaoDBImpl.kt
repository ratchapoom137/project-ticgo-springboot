package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.repository.SubtitleMovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SubtitleMovieDaoDBImpl: SubtitleMovieDao {
    @Autowired
    lateinit var subtitleRepository: SubtitleMovieRepository
}