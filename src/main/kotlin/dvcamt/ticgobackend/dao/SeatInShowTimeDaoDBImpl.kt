package dvcamt.ticgobackend.dao.dto

import dvcamt.ticgobackend.entity.SeatInShowTime
import dvcamt.ticgobackend.repository.SeatInShowTimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SeatInShowTimeDaoDBImpl: SeatInShowTimeDao {
    override fun save(seat: SeatInShowTime): SeatInShowTime {
        return seatInShowTimeRepository.save(seat)
    }

    override fun findById(id: Long?): SeatInShowTime {
        return seatInShowTimeRepository.findById(id!!).orElse(null)
    }

    @Autowired
    lateinit var seatInShowTimeRepository: SeatInShowTimeRepository
}